package org.wizardbuilder.processor

import com.google.auto.service.AutoService
import com.squareup.javapoet.ClassName
import com.squareup.javapoet.TypeName
import org.jetbrains.annotations.Nullable
import org.wizardbuilder.annotations.WizardBuilder
import org.wizardbuilder.core.BuilderGenerator
import org.wizardbuilder.core.BuilderSpecification
import org.wizardbuilder.core.ConstructorParameter
import org.wizardbuilder.core.impl.JavaPoetBuilderGenerator
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.annotation.processing.SupportedSourceVersion
import javax.lang.model.SourceVersion
import javax.lang.model.element.*
import javax.tools.Diagnostic

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor::class)
internal class WizardBuilderAnnotationProcessor : AbstractProcessor() {

    private val builderGenerator: BuilderGenerator = JavaPoetBuilderGenerator()

    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        val configured = roundEnv.getConfiguredElements()
        val annotated = roundEnv.getElementsAnnotatedWith(WizardBuilder::class.java)
        (configured + annotated).map { it.toBuilderSpecification() }
            .forEach { createSourceFile(specification = it) }
        return false
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(WizardBuilder::class.java.name)
    }

    override fun getSupportedOptions(): MutableSet<String> {
        return mutableSetOf(
            Options.NULLABLE_ANNOTATION,
            Options.NOT_NULL_ANNOTATION,
            Options.DEFAULT_BUILDER_SUFFIX,
            Options.DEFAULT_BUILDER_PACKAGE,
            Options.CREATE_BUILDER_FOR,
        )
    }

    private fun RoundEnvironment.getConfiguredElements(): List<Element> {
        val classNames = (processingEnv.options[Options.CREATE_BUILDER_FOR] ?: "").split(" ").map { it.trim() }
        return classNames.mapNotNull { className ->
            this.rootElements.find { it.kind == ElementKind.CLASS && it.asType().toString() == className }
        }
    }

    private fun getNullableAnnotationClass(): Class<Annotation>? {
        val className = processingEnv.options[Options.NULLABLE_ANNOTATION]
        return if (className == null) null else Class.forName(className).asAnnotationClass()
    }

    private fun getNotNullAnnotationClass(): Class<Annotation>? {
        val className = processingEnv.options[Options.NOT_NULL_ANNOTATION]
        return if (className == null) null else Class.forName(className).asAnnotationClass()
    }

    private fun getDefaultBuilderPackage(): String? {
        return processingEnv.options[Options.DEFAULT_BUILDER_PACKAGE]
    }

    @Suppress("UNCHECKED_CAST")
    private fun Class<*>.asAnnotationClass(): Class<Annotation> {
        require(this.isAnnotation)
        return this as Class<Annotation>
    }

    private fun getBuilderSuffix(): String {
        return processingEnv.options[Options.DEFAULT_BUILDER_SUFFIX] ?: "Builder"
    }

    private fun Element.getBuilderName(annotatedClassName: ClassName): String {
        return getCustomBuilderName() ?: "${annotatedClassName.simpleName()}${getBuilderSuffix()}"
    }

    private fun Element.getBuilderPackage(annotatedClassName: ClassName): String {
        return getCustomBuilderPackage() ?: getDefaultBuilderPackage() ?: annotatedClassName.packageName()
    }

    private fun Element.toBuilderSpecification(): BuilderSpecification {
        val (annotatedClassName, constructor) = getAnnotatedClassNameAndConstructor()
        return BuilderSpecification(
            forClass = annotatedClassName,
            name = ClassName.get(
                getBuilderPackage(annotatedClassName = annotatedClassName),
                getBuilderName(annotatedClassName = annotatedClassName),
            ),
            parameters = constructor.getConstructorParameters(),
            notNullAnnotation = getNotNullAnnotationClass(),
            nullableAnnotation = getNullableAnnotationClass(),
        )
    }

    private fun createSourceFile(specification: BuilderSpecification) {
        processingEnv.filer.createSourceFile(specification.name.toString()).openWriter().use {
            val sources = builderGenerator.generate(specification)
            it.write(sources)
        }
    }

    private fun ExecutableElement.getConstructorParameters(): List<ConstructorParameter> {
        return this.parameters.mapIndexed { index, parameter ->
            ConstructorParameter(
                name = parameter.simpleName.toString(),
                type = TypeName.get(parameter.asType()),
                isNullable = parameter.isNullable(),
                isVarArgs = isVarArgs && index == parameters.lastIndex,
            )
        }
    }

    private fun VariableElement.isNullable(): Boolean {
        val nullableAnnotation = getNullableAnnotationClass() ?: Nullable::class.java
        return this.getAnnotation(nullableAnnotation) != null
    }

    private fun Element.getCustomBuilderName(): String? {
        val builderAnnotation: WizardBuilder? = this.getAnnotation(WizardBuilder::class.java)
        return if (builderAnnotation == null || builderAnnotation.name == WizardBuilder.DEFAULT_VALUE) null else builderAnnotation.name
    }

    private fun Element.getCustomBuilderPackage(): String? {
        val builderAnnotation: WizardBuilder? = this.getAnnotation(WizardBuilder::class.java)
        return if (builderAnnotation == null || builderAnnotation.packageName == WizardBuilder.DEFAULT_VALUE) null else builderAnnotation.packageName
    }

    private fun Element.toClassName(): ClassName {
        require(this.kind == ElementKind.CLASS)
        return ClassName.get(this.asType()) as ClassName
    }

    private fun Element.getAnnotatedClassNameAndConstructor(): Pair<ClassName, ExecutableElement> {
        return when (this.kind) {
            ElementKind.CLASS -> {
                val constructors = this.enclosedElements.filter { it.kind == ElementKind.CONSTRUCTOR }
                if (constructors.size != 1) {
                    printError("Found multiple constructors. Please annotate one specific constructor instead of the class itself.")
                }
                Pair(this.toClassName(), constructors.first() as ExecutableElement)
            }
            ElementKind.CONSTRUCTOR -> Pair(this.enclosingElement.toClassName(), this as ExecutableElement)
            else -> throw IllegalArgumentException()
        }
    }

    private fun printError(value: String) {
        processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, value)
    }

    private companion object {
        object Options {
            private const val PREFIX = "wizardbuilder"
            const val NULLABLE_ANNOTATION = "$PREFIX.nullableAnnotation"
            const val NOT_NULL_ANNOTATION = "$PREFIX.notNullAnnotation"
            const val DEFAULT_BUILDER_SUFFIX = "$PREFIX.defaultBuilderSuffix"
            const val DEFAULT_BUILDER_PACKAGE = "$PREFIX.defaultBuilderPackage"
            const val CREATE_BUILDER_FOR = "$PREFIX.createBuilderFor"
        }
    }

}
