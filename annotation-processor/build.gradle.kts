plugins {
    kotlin("jvm") version "1.5.10"
    kotlin("kapt") version "1.5.10"
    id("maven-publish")
}

group = "org.wizardbuilder"
version = "0.0.1-experimental"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(project(":core"))
    implementation(project(":annotations"))
    implementation("com.squareup:javapoet:1.9.0")
    implementation("com.google.auto.service:auto-service:1.0")
    kapt("com.google.auto.service:auto-service:1.0")
}

java {
    withSourcesJar()
}

publishing {
    val gitlabProjectId = "27558766"
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
