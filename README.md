# WizardBuilder

This project is an annotation processor that can create Java builder classes for Java and Kotlin classes. The created builders provide fluent
interfaces but only provide access to the next build step. This way no mandatory properties can be missing when the object is built.

## How to get it

1. Add the kapt plugin

```kotlin
plugins {
    // ...
    kotlin("kapt") version "1.5.10"
    // ...
}   
```

2. Add the projects GitLab repository to your build.gradle.kts:

```kotlin
repositories {
    // ...
    maven {
        url = uri("https://gitlab.com/api/v4/projects/27558766/packages/maven")
    }
    // ...
}
```

2. Add the dependencies:

````kotlin
dependencies {
    // ...
    implementation("org.wizardbuilder:annotations:0.0.1-experimental")
    kapt("org.wizardbuilder:annotation-processor:0.0.1-experimental")
    // ...
}
````

## How to use it

```kotlin
package org.wizardbuilder.example

import org.wizardbuilder.annotations.WizardBuilder
import java.time.LocalDate

@WizardBuilder
data class Student(
    val firstName: String,
    val lastName: String,
    val number: String,
    val dateOfBirth: LocalDate?,
    val ects: Int,
    val matriculationDate: LocalDate,
    val course: String,
)

```

```java
class Application {

  public static void main(String[] args) {
    Student student = StudentBuilder.create()
        .firstName("Max")
        .lastName("Mustermann")
        .number("12345678")
        .ects(150)
        .matriculationDate(LocalDate.of(2015, Month.APRIL, 12))
        .course("Business informatics")
        .build();
  }

} 
```
