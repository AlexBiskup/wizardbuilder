package org.wizardbuilder.core

import com.squareup.javapoet.ClassName

data class BuilderSpecification(
    val forClass: ClassName,
    val name: ClassName = ClassName.get(forClass.packageName(), "${forClass.simpleName()}Builder"),
    val parameters: List<ConstructorParameter>,
    val notNullAnnotation: Class<*>? = null,
    val nullableAnnotation: Class<*>? = null,
)
