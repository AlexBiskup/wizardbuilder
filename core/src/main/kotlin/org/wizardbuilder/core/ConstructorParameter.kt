package org.wizardbuilder.core

import com.squareup.javapoet.TypeName

data class ConstructorParameter(val name: String, val type: TypeName, val isNullable: Boolean, val isVarArgs: Boolean = false) {

    fun isOptional() = isNullable

    fun isRequired() = !isOptional()

}




