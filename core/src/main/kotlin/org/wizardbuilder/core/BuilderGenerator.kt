package org.wizardbuilder.core

interface BuilderGenerator {

    fun generate(specification: BuilderSpecification): String

}

