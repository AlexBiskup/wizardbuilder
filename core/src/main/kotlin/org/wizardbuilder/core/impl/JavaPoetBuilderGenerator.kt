package org.wizardbuilder.core.impl

import org.wizardbuilder.core.BuilderGenerator
import org.wizardbuilder.core.BuilderSpecification


class JavaPoetBuilderGenerator : BuilderGenerator {

    override fun generate(specification: BuilderSpecification): String {
        return WizardBuilder(specification).generate()
    }

}







