package org.wizardbuilder.core.impl

import com.squareup.javapoet.*
import org.wizardbuilder.core.BuilderSpecification
import org.wizardbuilder.core.ConstructorParameter
import java.util.*
import javax.lang.model.element.Modifier


internal class WizardBuilder(private val specification: BuilderSpecification) {

    private val rootClassBuilder = TypeSpec.classBuilder(specification.name).addModifiers(Modifier.PUBLIC, Modifier.FINAL)

    private val buildSteps = this.specification.parameters.toBuildSteps()

    fun generate(): String {
        for (interfaceSpec in buildSteps.interfaceSpecs) rootClassBuilder.addType(interfaceSpec)
        rootClassBuilder.addType(createWizardClassSpec())
        rootClassBuilder.addMethod(createStaticFactoryMethodSpec())
        val file = JavaFile.builder(specification.name.packageName(), rootClassBuilder.build())
        return file.build().asText()
    }

    private fun createWizardClassSpec(): TypeSpec {
        val builder = TypeSpec.classBuilder("Wizard").addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
        for (interfaceSpec in buildSteps.interfaceSpecs) builder.addSuperinterface(interfaceSpec.typeName)
        for (fieldSpec in buildSteps.fieldSpecs) builder.addField(fieldSpec)
        for (overrideSpec in buildSteps.overrideSpecs) builder.addMethod(overrideSpec)
        builder.addMethod(MethodSpec.constructorBuilder().addModifiers(Modifier.PRIVATE).build())
        return builder.build()
    }

    private fun createStaticFactoryMethodSpec(): MethodSpec {
        val builder = MethodSpec.methodBuilder("create")
            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
            .returns(buildSteps.first().typeName)
            .addStatement("return new Wizard()")
        if (specification.notNullAnnotation != null) builder.addAnnotation(specification.notNullAnnotation)
        return builder.build()
    }

    private fun List<ConstructorParameter>.toBuildSteps(): List<BuildStep> {
        val optional = this.filter { it.isOptional() }
        val lastBuildStep = LastBuildStep(constructorParameters = optional)
        return this
            .filter { it.isRequired() }
            .reversed()
            .mapWithPrevious<ConstructorParameter, BuildStep> { constructorParameter, previousBuildStep ->
                ParameterBuildStep(constructorParameter = constructorParameter, nextStep = previousBuildStep ?: lastBuildStep)
            }.reversed() + lastBuildStep
    }

    private val List<BuildStep>.interfaceSpecs: List<TypeSpec> get() = this.map { it.interfaceSpec }

    private val List<BuildStep>.fieldSpecs: List<FieldSpec> get() = this.flatMap { it.fieldSpecs }

    private val List<BuildStep>.overrideSpecs: List<MethodSpec> get() = this.flatMap { it.overrideSpecs }

    private val TypeSpec.typeName: TypeName
        get() = ClassName.get(specification.name.packageName(), specification.name.simpleName(), this.name)

    private abstract inner class BuildStep {

        abstract val interfaceSpec: TypeSpec

        abstract val overrideSpecs: List<MethodSpec>

        abstract val fieldSpecs: List<FieldSpec>

        val typeName: TypeName
            get() = interfaceSpec.typeName


        protected fun ConstructorParameter.toAbstractMethodSpec(returns: TypeName, isParameterOptional: Boolean): MethodSpec {
            val builder = MethodSpec.methodBuilder(this.name)
                .addParameter(this.getValueParameter(isOptional = isParameterOptional, addAnnotations = true))
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .varargs(isVarArgs)
                .returns(returns)
            if (specification.notNullAnnotation != null) builder.addAnnotation(specification.notNullAnnotation)
            return builder.build()
        }

        protected fun ConstructorParameter.toOverrideMethodSpec(returns: TypeName, isParameterOptional: Boolean): MethodSpec {
            val valueParameterName = getValueParameterName()
            val assignmentStatement = if (isVarArgs) {
                "this.${this.name} = \$T.asList($valueParameterName).toArray(new \$T{})"
            } else {
                "this.${this.name} = $valueParameterName"
            }
            return MethodSpec.methodBuilder(this.name)
                .addAnnotation(Override::class.java)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(this.getValueParameter(isOptional = isParameterOptional, addAnnotations = false))
                .addStatement(assignmentStatement, Arrays::class.java, this.type)
                .addStatement("return this")
                .varargs(isVarArgs)
                .returns(returns)
                .build()
        }

        protected fun ConstructorParameter.toFieldSpec(): FieldSpec {
            return FieldSpec.builder(this.type, this.name, Modifier.PRIVATE).build()
        }

        private fun ConstructorParameter.getValueParameter(isOptional: Boolean, addAnnotations: Boolean): ParameterSpec {
            val builder = ParameterSpec.builder(this.type, getValueParameterName())
            if (addAnnotations && !type.isPrimitive) {
                val annotation = if (isOptional) specification.nullableAnnotation else specification.notNullAnnotation
                if (annotation != null) builder.addAnnotation(annotation)
            }
            return builder.build()
        }

        private fun ConstructorParameter.getValueParameterName() = if (isVarArgs) "values" else "value"

    }

    private inner class LastBuildStep(private val constructorParameters: List<ConstructorParameter>) : BuildStep() {

        override val interfaceSpec: TypeSpec by lazy {
            val name = "BuildStep"
            val typeName = ClassName.get(specification.name.packageName(), specification.name.simpleName(), name)
            val parameterMethods = constructorParameters.map { it.toAbstractMethodSpec(returns = typeName, isParameterOptional = true) }
            val buildMethod = createAbstractBuildMethodSpec()
            TypeSpec.interfaceBuilder(name).addMethods(parameterMethods + buildMethod).addModifiers(Modifier.PUBLIC).build()
        }

        override val overrideSpecs: List<MethodSpec> by lazy {
            val parameterMethods = constructorParameters.map { it.toOverrideMethodSpec(returns = this.typeName, isParameterOptional = true) }
            val buildMethod = createOverrideBuildMethodSpec()
            parameterMethods + buildMethod
        }

        override val fieldSpecs: List<FieldSpec> by lazy {
            constructorParameters.map { it.toFieldSpec() }
        }

        private fun createAbstractBuildMethodSpec(): MethodSpec {
            val builder = MethodSpec.methodBuilder("build").addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(specification.forClass)
            if (specification.notNullAnnotation != null) builder.addAnnotation(specification.notNullAnnotation)
            return builder.build()
        }

        private fun createOverrideBuildMethodSpec(): MethodSpec {
            val parameters = this@WizardBuilder.specification.parameters.joinToString(separator = ", ") { "this.${it.name}" }

            return MethodSpec.methodBuilder("build")
                .addAnnotation(Override::class.java)
                .addModifiers(Modifier.PUBLIC)
                .addStatement("return new \$T($parameters)", specification.forClass)
                .returns(specification.forClass)
                .build()
        }

    }

    private inner class ParameterBuildStep(
        private val constructorParameter: ConstructorParameter,
        nextStep: BuildStep
    ) : BuildStep() {

        private val returns = nextStep.typeName

        override val interfaceSpec: TypeSpec by lazy {
            val name = "${constructorParameter.name.capitalize()}Step"
            TypeSpec.interfaceBuilder(name)
                .addMethod(constructorParameter.toAbstractMethodSpec(returns = returns, isParameterOptional = false))
                .addModifiers(Modifier.PUBLIC)
                .build()
        }

        override val overrideSpecs: List<MethodSpec> by lazy {
            val method = constructorParameter.toOverrideMethodSpec(returns = returns, isParameterOptional = false)
            listOf(method)
        }

        override val fieldSpecs: List<FieldSpec> by lazy {
            listOf(constructorParameter.toFieldSpec())
        }

    }

}


