package org.wizardbuilder.core.impl

internal inline fun <T, R> Iterable<T>.mapWithPrevious(transform: (T, R?) -> R): List<R> {
    var previous: R? = null
    return map {
        val result = transform(it, previous)
        previous = result
        result
    }
}
