package org.wizardbuilder.core.impl

import com.squareup.javapoet.JavaFile
import java.io.ByteArrayOutputStream
import java.io.PrintStream

internal fun JavaFile.asText(): String {
    val out = ByteArrayOutputStream()
    val stream = PrintStream(out)
    this.writeTo(stream)
    return String(out.toByteArray())
}
