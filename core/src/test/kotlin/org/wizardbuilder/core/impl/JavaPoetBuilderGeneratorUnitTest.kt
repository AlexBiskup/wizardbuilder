package org.wizardbuilder.core.impl

import com.squareup.javapoet.ArrayTypeName
import com.squareup.javapoet.ClassName
import com.squareup.javapoet.TypeName
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.wizardbuilder.core.BuilderGenerator
import org.wizardbuilder.core.BuilderSpecification
import org.wizardbuilder.core.ConstructorParameter

internal class JavaPoetBuilderGeneratorUnitTest {

    private val generator: BuilderGenerator = JavaPoetBuilderGenerator()

    @Test
    fun `when creating - for specification with no nullable parameters - creates correct sources`() {
        val expected = this::class.java.classLoader?.getResource("KStudentBuilder")?.readText()
        val specification = BuilderSpecification(
            forClass = ClassName.get("org.wizardbuilder", "KStudent"),
            parameters = listOf(
                ConstructorParameter(name = "name", type = ClassName.get(String::class.java), isNullable = false),
                ConstructorParameter(name = "age", type = TypeName.INT, isNullable = false),
            ),
        )
        val result = generator.generate(specification)
        assertEquals(expected, result)
    }

    @Test
    fun `when creating - for specification with nullable parameters - creates correct sources`() {
        val expected = this::class.java.classLoader?.getResource("KPersonBuilder")?.readText()
        val specification = BuilderSpecification(
            forClass = ClassName.get("org.wizardbuilder", "KPerson"),
            parameters = listOf(
                ConstructorParameter(name = "name", type = ClassName.get(String::class.java), isNullable = false),
                ConstructorParameter(name = "personnelNumber", type = ClassName.get(String::class.java), isNullable = true),
                ConstructorParameter(name = "isActive", TypeName.BOOLEAN, isNullable = false),
                ConstructorParameter(name = "age", TypeName.INT, isNullable = false),
            ),
        )
        val result = generator.generate(specification)
        assertEquals(expected, result)
    }

    @Test
    fun `when creating - for specification with nullable parameters and jetbrains annotations - creates correct sources`() {
        val expected = this::class.java.classLoader?.getResource("KPersonBuilderNullAnnotations")?.readText()
        val specification = BuilderSpecification(
            forClass = ClassName.get("org.wizardbuilder", "KPerson"),
            parameters = listOf(
                ConstructorParameter(name = "name", type = ClassName.get(String::class.java), isNullable = false),
                ConstructorParameter(name = "personnelNumber", type = ClassName.get(String::class.java), isNullable = true),
                ConstructorParameter(name = "isActive", TypeName.BOOLEAN, isNullable = false),
                ConstructorParameter(name = "age", TypeName.INT, isNullable = false),
            ),
            nullableAnnotation = Nullable::class.java,
            notNullAnnotation = NotNull::class.java,
        )
        val result = generator.generate(specification)
        assertEquals(expected, result)
    }

    @Test
    fun `when creating - with vararg- creates correct sources`() {
        val expected = this::class.java.classLoader?.getResource("KPersonBuilderVararg")?.readText()
        val specification = BuilderSpecification(
            forClass = ClassName.get("org.wizardbuilder", "KPerson"),
            parameters = listOf(
                ConstructorParameter(name = "name", type = ClassName.get(String::class.java), isNullable = false),
                ConstructorParameter(name = "friends", ArrayTypeName.of(ClassName.get(String::class.java)), isNullable = false, isVarArgs = true),
            ),
        )
        val result = generator.generate(specification)
        assertEquals(expected, result)
    }

}
