package org.wizardbuilder.annotations

@Target(AnnotationTarget.CLASS, AnnotationTarget.CONSTRUCTOR)
@Retention(AnnotationRetention.SOURCE)
annotation class WizardBuilder(val name: String = DEFAULT_VALUE, val packageName: String = DEFAULT_VALUE) {

    companion object {
        const val DEFAULT_VALUE = "default"
    }

}
